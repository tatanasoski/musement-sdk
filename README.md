
# Musement SDK

Tool for easy accessing Musement SDK

## Getting Started

You will have to have permission for accessing this tool from the repository

### Installing

Adding it to your composer file

```
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "tatanasoski/musement-sdk",
            "version": "0.0.2",
            "source": {
                "url": "git@bitbucket.org:tatanasoski/musement-sdk.git",
                "type": "git",
                "reference": "master"
            },
            "autoload": {
                "psr-4" : {
                    "MusementSdk\\" : "src/"
                }
            }
        }
    }]
```

And add in the required

```
"require": {
    "tatanasoski/musement-sdk": "^0.0.2"
}
```

### Code Example

```
require __DIR__ . '/vendor/autoload.php';

$musementSdk = new MusementSdk();
//Geting all locales
$locales = $musementSdk->getLocales()->getAll();

//Geting paginated cities
$filter     = $musementSdk->getFilter();
$filter->set('filterField', 'filterValue');
$pagination = $musementSdk->getPagination();
$pagination->setPage(1);
$pagination->setLimit(10);
$cities = $musementSdk->getCities()->getAllBy($filter, $pagination);

```
