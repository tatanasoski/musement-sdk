<?php
namespace MusementSdk\Interfaces;
/**
 * Interface handling response
 */
interface HttpResponseInterface {
    public function setResponse($response);
    public function toArray();
    public function toObject();
}
