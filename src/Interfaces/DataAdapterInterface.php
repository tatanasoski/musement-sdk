<?php
namespace MusementSdk\Interfaces;
/**
 * Interface for making a request to data source
 */
interface DataAdapterInterface {
    public function request($destination);
}
