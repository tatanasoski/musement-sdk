<?php
namespace MusementSdk\Interfaces;
use MusementSdk\Libraries\Pagination;
use MusementSdk\Libraries\Filter;
/**
 * Interface for resources
 */
interface ResourceInterface {
    public function setPagination(Pagination $pagination);
    public function getPagination();
    public function setFilter(Filter $filter);
    public function getFilter();
    public function getAllBy();
    public function getById($id);
    
}
