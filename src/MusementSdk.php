<?php
namespace MusementSdk;
use \Psr\Container\ContainerInterface;
use \DI\ContainerBuilder;
use MusementSdk\Adapters\HttpClientDataAdapter;
use MusementSdk\Adapters\HttpJsonResponseAdapter;
use MusementSdk\Adapters\HttpXmlResponseAdapter;
use MusementSdk\Adapters\FileJsonDataAdapter;

use MusementSdk\Clients\HttpClient;

/**
 * Sdk for easier access to musement api
 */
class MusementSdk {
    
    /**
     * @var ContainerInterface 
     */
    private $diContainer;
    
    /**
     * @var Interfaces\ResourceInterface
     */
    private $locales;
    
    /**
     * @var Interfaces\ResourceInterface
     */
    private $cities;
    
    /**
     * @var Interfaces\ResourceInterface
     */
    private $activities;
    
    public function __construct($baseUrl = null) {
        
        $baseUrl = $baseUrl ?: 'https://api.musement.com/api/';
        
        $builder = new ContainerBuilder();
        $builder->addDefinitions([
            'XmlResponseAdapter' => new HttpXmlResponseAdapter(),
            'JsonResponseAdapter'=> new HttpJsonResponseAdapter(),
            'HttpService' => new \GuzzleHttp\Client(['base_uri' => $baseUrl]),
            'HttpAdapter' => function(ContainerInterface $c){
                return new HttpClientDataAdapter($c->get('HttpService'));
            },
            'FileAdapter' => new FileJsonDataAdapter(),
            'Locales'   => function(ContainerInterface $c) {
                return new Resources\Locales(
                        $c->get('FileAdapter'),
                        $c->get('XmlResponseAdapter'),
                        $c->get('JsonResponseAdapter')
                );
            },
            'Cities'    => function(ContainerInterface $c) {
                return new Resources\Cities(
                        $c->get('HttpAdapter'),
                        $c->get('XmlResponseAdapter'),
                        $c->get('JsonResponseAdapter')
                );
            },
            'Activities'    => function(ContainerInterface $c) {
                return new Resources\Activities(
                        $c->get('HttpAdapter'),
                        $c->get('XmlResponseAdapter'),
                        $c->get('JsonResponseAdapter')
                );
            }
            
        ]);
        $this->diContainer  = $builder->build();
        
        $this->cities       = $this->diContainer->get('Cities');
        $this->activities   = $this->diContainer->get('Activities');
        $this->locales      = $this->diContainer->get('Locales');
    }
    
    /**
     * Function for getting Cities resource
     * @return Resources\Cities
     */
    public function getCities(){
        return $this->cities;
    }
    
    /**
     * Function for getting Activities resource
     * @return Resources\Activities
     */
    public function getActivities(){
        return $this->activities;
    }
    
    /**
     * Function for getting Locales resource
     * @return Resources\Locales
     */
    public function getLocales(){
        return $this->locales;
    }
    
    /**
     * Function for getting new Filter object
     * @return \MusementSdk\Libraries\Filter
     */
    public function getFilter(){
        return new Libraries\Filter();
    }
    
    /**
     * Function for getting new Pagination object
     * @return \MusementSdk\Libraries\Pagination
     */
    public function getPagination(){
        return new Libraries\Pagination();
    }
}
