<?php
namespace MusementSdk\Adapters;

use \MusementSdk\Interfaces\HttpResponseInterface;
/**
 * Class for parsing json response
 */
class HttpJsonResponseAdapter implements  HttpResponseInterface{
    /**
     * @var string 
     */
    private $response;
    
    /**
     * Function for setting the response, returns itself for chaining
     * @param type $response
     * @return \MusementSdk\Adapters\HttpJsonResponseAdapter
     */
    public function setResponse($response) {
        $this->response = $response;
        return $this;
    }

    /**
     * Function for decoding the response to array
     * @return array
     */
    public function toArray() {
        return json_decode($this->response, true);
    }

    /**
     * Function for decoding te response to object
     * @return \stdClass
     */
    public function toObject() {
        return json_decode($this->response);
    }
}
