<?php
namespace MusementSdk\Adapters;

use MusementSdk\Interfaces\DataAdapterInterface;

/**
 * Class for getting data from http request
 */
class HttpClientDataAdapter implements DataAdapterInterface{
    
    /**
     *Http client
     * @var type 
     */
    private $httpClient;
    
    public function __construct($httpClient) {
        $this->httpClient = $httpClient;
    }
    
    /**
     * Function for setting body type
     * @param type $method
     * @return type
     */
    private function getBodyTypeByMethod($method) {
        switch ($method){
            case 'POST':
            case 'PATCH':
            case 'PUT':
                $bodyType = \GuzzleHttp\RequestOptions::FORM_PARAMS;
                break;
            default:
            case 'GET':
                $bodyType = \GuzzleHttp\RequestOptions::QUERY;
                break;
        }
        return $bodyType;
    }
    
    /**
     * Function for making the request
     * @param string $destination
     * @param string $method
     * @param array $params
     * @param array $headers
     * @return json
     */
    public function request($destination, $method = 'GET', $params = [], $headers = []) {
        return $this->httpClient
                ->request($method, $destination, 
                    [
                        $this->getBodyTypeByMethod($method) => $params,
                        'headers' => $headers
                    ]
                )
                ->getBody()
                ->getContents();
    }

}
