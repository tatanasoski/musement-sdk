<?php
namespace MusementSdk\Adapters;

use MusementSdk\Interfaces\DataAdapterInterface;

/**
 * Class for getting data
 */
class FileJsonDataAdapter implements DataAdapterInterface{    
   
    /**
     * Function for getting data from a file destination
     * @param type $destination
     * @return type
     */
    public function request($destination) {
        return file_get_contents($destination);
    }

}
