<?php
namespace MusementSdk\Adapters;
use \MusementSdk\Interfaces\HttpResponseInterface;

/**
 * Class for parsing xml response
 */
class HttpXmlResponseAdapter implements HttpResponseInterface{
    /**
     * @var string 
     */
    private $response;
    
    /**
     * Function for setting the response, returns itself for chaining
     * @param type $response
     * @return \MusementSdk\Adapters\HttpJsonResponseAdapter
     */
    public function setResponse($response) {
        $this->response = $response;
        return $this;
    }

    /**
     * Function for decoding the response to array
     * @return array
     */
    public function toArray() {
        $data = \simplexml_load_string($this->response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = \json_encode($data);
        return \json_decode($json, true);
    }

    /**
     * Function for decoding te response to object
     * @return \stdClass
     */
    public function toObject() {
        $data = \simplexml_load_string($this->response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = \json_encode($data);
        return \json_decode($json);
    }
}
