<?php
 namespace MusementSdk\Entities;
 /**
  * Entity class for Activity
  */
 class Activity extends EntityAbstract{
     
     /**
      * @var string 
      */
     public $sTitle;
     
     /**
      * @var string 
      */
     public $sUrl;
 }
