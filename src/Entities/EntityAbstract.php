<?php
namespace MusementSdk\Entities;

/**
 * Abstract entity class
 */
abstract class EntityAbstract {
    public function __construct(array $row) {
        foreach ($row as $parameter => $value) {
            if(property_exists($this, $parameter)) {
                $this->$parameter = $value;
            }
        }
    }
    
    public static function create(array $row) {
        $className = get_called_class();
        return new $className($row);
    }
}
