<?php
namespace MusementSdk\Entities;
 
/**
 * Entity class for City
 */
class City extends EntityAbstract{
    /**
     * @var int 
     */
    public $iId;
    
    /**
     * @var string 
     */
    public $sName;

    /**
     * @var string 
     */
    public $sUrl;
 }
