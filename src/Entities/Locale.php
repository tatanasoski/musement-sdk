<?php
 namespace MusementSdk\Entities;
 
 /**
 * Entity class for City
 */
 class Locale extends EntityAbstract{
     
     /**
     * @var string 
     */
     public $sTag;
     
     /**
     * @var string 
     */
     public $sName;
 }