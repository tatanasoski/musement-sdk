<?php
namespace MusementSdk\Resources;
use MusementSdk\Resources\ResourceAbstract;
use MusementSdk\Interfaces\ResourceInterface;
use \MusementSdk\Libraries\Filter;
use \MusementSdk\Libraries\Pagination;
use \MusementSdk\Entities\Activity;

/**
 * Resource class for Activities
 */
class Activities extends ResourceAbstract implements ResourceInterface{
    protected $source = 'activities';
    
    /**
     * Function for getting results by filter
     * @param Filter $filter
     * @param Pagination $pagination
     * @return array
     */
    public function getAllBy(Filter $filter = null, Pagination $pagination = null) {
        return $this->mapCollection(
                $this->getResponseParser('json')
                ->setResponse(parent::getAllBy($filter, $pagination))
                ->toArray());
    }
    
    /**
     * Function for returning activity by id
     * @param int $id
     * @return Activity
     */
    public function getById($id) {
        return $this->mapRow(
                $this->getResponseParser('json')
                ->setResponse(parent::getById($id))
                ->toArray());
    }
    
    /**
     * Function for returning all activities by city
     * @param int $cityId
     * @param Filter $filter
     * @param Pagination $pagination
     * @return array
     */
    public function getAllByCity($cityId, Filter $filter = null, Pagination $pagination = null) {
        return $this->mapCollection(
                $this->getResponseParser('json')
                ->setResponse(parent::getByRelation($cityId, 'cities', $filter, $pagination))
                ->toArray());
    }
    
    /**
     * Function for mapping the respurce with the entity
     * @param array $row
     * @return Activity
     */
    public function mapRow($row) {
        return Activity::create([
            'sTitle' => $row['title'],       
            'sUrl'   => $row['url']
        ]);
    }
}
