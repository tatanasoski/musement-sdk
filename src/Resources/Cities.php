<?php
namespace MusementSdk\Resources;
use MusementSdk\Resources\ResourceAbstract;
use MusementSdk\Interfaces\ResourceInterface;
use \MusementSdk\Libraries\Filter;
use \MusementSdk\Libraries\Pagination;
use MusementSdk\Entities\City;

/**
 * Resource class for Cities
 */
class Cities extends ResourceAbstract implements ResourceInterface{
    protected $source = 'cities';
    
    /**
     * Function for getting results by filter
     * @param Filter $filter
     * @param Pagination $pagination
     * @return array
     */
    public function getAllBy(Filter $filter = null, Pagination $pagination = null) {
        return $this->mapCollection(
                $this->getResponseParser('json')
                    ->setResponse(parent::getAllBy($filter, $pagination))
                    ->toArray()
        );
    }
    
    /**
     * Function for returning City by id
     * @param int $id
     * @return City
     */
    public function getById($id) {
        return $this->mapRow($this->getResponseParser('json')
                ->setResponse(parent::getById($id))
                ->toArray());
    }
     
    /**
     * Function for mapping the respurce with the entity
     * @param array $row
     * @return City
     */
    public function mapRow($row) {
        return City::create([
            'iId'   => $row['id'],
            'sName' => $row['name'],             
            'sUrl'  => $row['url']
        ]);
    }     
}
