<?php
namespace MusementSdk\Resources;

use MusementSdk\Interfaces\DataAdapterInterface;
use MusementSdk\Interfaces\HttpResponseInterface;

use MusementSdk\Libraries\Filter;
use MusementSdk\Libraries\Pagination;

/**
 * Abstract class for resources
 */
abstract class ResourceAbstract {
    /**
     * Version of the api
     * @var string 
     */
    private static $version = 'v3';
    
    /**
     * @var Pagination
     */
    protected $pagination;
    
    /**
     * @var Filter
     */
    protected $filter;    
    
    /**
     * @var string
     */
    protected $source;
    
    /**
     * @var DataAdapterInterface
     */
    protected $dataAdapter;
    
    /**
     * @var array 
     */
    protected $responseParsers;
    
    public function __construct(
            DataAdapterInterface $dataAdapter,
            HttpResponseInterface $responseXmlParser,
            HttpResponseInterface $responseJsonParser
            ) {
        $this->dataAdapter                = $dataAdapter;
        $this->responseParsers['xml']    = $responseXmlParser;
        $this->responseParsers['json']   = $responseJsonParser;
    }
    
    /**
     * Function for setting filter
     * @param Filter $filter
     */
    public function setFilter(Filter $filter) {
        $this->filter = $filter;
    }
    
    /**
     * Function for getting filter
     * @return Filter
     */
    public function getFilter() {
        return $this->filter;
    }
    
    /**
     * Function for setting pagination
     * @param Pagination $pagination
     */
    public function setPagination(Pagination $pagination) {
        $this->pagination = $pagination;
    }
    
    /**
     * Function for getting pagination
     * @return Pagination
     */
    public function getPagination() {
        return $this->pagination;
    }
    
    /**
     * @param string $type Xml | json
     * @return HttpResponseInterface
     */
    public function getResponseParser($type){
        return $this->responseParsers[$type]; 
    }
    
    /**
     * Function for returning paginated data by filter
     * @param Filter $filter
     * @param Pagination $pagination
     * @return json
     */
    protected function getAllBy(Filter $filter = null, Pagination $pagination = null) {
        $data       = [];
        $headers    = [];
        if(!is_null($pagination)) {
            $data = array_merge($data, $pagination->getParams());
        }
        if(!is_null($filter)) {
            $data = array_merge($data, $filter->getParams());
            $headers = $filter->getHeaderParams();
        }
        return $this->dataAdapter->request(self::$version.'/'.$this->source, 'GET', $data, $headers);
    }
    
    /**
     * Function for returning data by identifier
     * @param mixed $id
     * @return json
     */
    protected function getById($id) {
        return $this->dataAdapter->request(self::$version.'/'.$this->source.$id, 'GET');
    }
    
    /**
     * Function for returning data by relation
     * @param mixed $id
     * @param string $relation
     * @param Filter $filter
     * @param Pagination $pagination
     * @return json
     */
    protected function getByRelation($id, $relation, Filter $filter = null, Pagination $pagination = null) {
        
        $data    = [];
        $headers = [];
        if(!is_null($pagination)) {
            $data = array_merge($data, $pagination->getParams());
        }
        if(!is_null($filter)) {
            $data = array_merge($data, $filter->getParams());
            $headers = $filter->getHeaderParams();
        }
        return $this->dataAdapter->request(self::$version.'/'.$relation.'/'.$id.'/'.$this->source, 'GET', $data, $headers);
    }
    
    /**
     * Function for mapping collection
     * @param array $collection
     * @return array
     */
    protected function mapCollection($collection){
         $respCollection = [];
         foreach($collection as $data) {
             $respCollection[] = $this->mapRow($data);
         }
         return $respCollection;
     }
}
