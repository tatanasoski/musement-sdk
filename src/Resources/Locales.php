<?php
namespace MusementSdk\Resources;
use MusementSdk\Resources\ResourceAbstract;
use MusementSdk\Entities\Locale;
use MusementSdk\Interfaces\DataAdapterInterface;
use \MusementSdk\Interfaces\HttpResponseInterface;
use MusementSdk\Libraries\Filter;
use MusementSdk\Libraries\Pagination;

/**
 * Resource class for Cities
 */
class Locales extends ResourceAbstract {
    
    public function __construct(
            DataAdapterInterface $dataAdapter, 
            HttpResponseInterface $responseXmlParser, 
            HttpResponseInterface $responseJsonParser) {
        
        $this->source = rtrim(dirname(__FILE__), '/src/Resources').'/data/locales.json';
        parent::__construct($dataAdapter, $responseXmlParser, $responseJsonParser);
    }
    
    /**
     * Function for getting all results
     * @return array
     */
    public function getAll() {
        return $this->mapCollection(
                $this->getResponseParser('json')
                    ->setResponse($this->dataAdapter->request($this->source))
                    ->toArray()
        );
    }
    
    /**
     * Function for returning Locale by tag
     * @param string $id
     * @return Locale
     */
    public function getById($id) {
        $resp = [];
        foreach($this->getAll() as $obj) {
            if ($id == $obj->sTag) {
                $resp = $obj;
                break;
            }
        }
        
        return $resp;
    }
    
    /**
     * Function for getting results by filter
     * @param Filter $filter
     * @param Pagination $pagination
     * @return array
     */
    public function getAllBy(Filter $filter = null, Pagination $pagination = null) {
        if (!empty($pagination)) {
            return array_slice($this->getAll(), $pagination->getOffset(), $pagination->getLimit());
        } else {
            return $this->getAll();
        }
        
    }
     
    /**
     * Function for mapping the respurce with the entity
     * @param array $row
     * @return Locale
     */
    public function mapRow($row) {
        return Locale::create([
            'sTag'   => $row['tag'],
            'sName' => $row['name']
        ]);
    }     
}
