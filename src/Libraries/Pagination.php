<?php
namespace MusementSdk\Libraries;

/**
 * Pagination class library
 */
class Pagination {
    /**
     * @var int
     */
    private $page = 1;
    
    /**
     * @var int
     */
    private $limit = 10;
    
    /**
     * @var int
     */
    private $offset;
    
    /**
     * Function for setting page
     * @param int $page
     */
    public function setPage($page) {
        $this->page = $page;
    }
    
    /**
     * Function for setting offset
     * @param int $offset
     */
    public function setOffset($offset) {
        $this->offset = $offset;
    }
    
    /**
     * Function for setting limit
     * @param int $limit
     */
    public function setLimit($limit) {
        $this->limit = $limit;
    } 
    
    /**
     * Function for getting page
     * @return int
     */
    public function getPage() {
        return $this->page;
    }
    
    /**
     * Function for getting limit
     * @return int
     */
    public function getLimit() {
        return $this->limit;
    }
    
    /**
     * Function for getting offset
     * @return int
     */
    public function getOffset() {
        return $this->offset ?: ($this->getPage() - 1) * $this->getLimit();
    }
    
    /**
     * Function for getting array('offset' => int, 'limit' => int)
     * @return array
     */
    public function getParams() {
        return [
            'offset' => $this->getOffset(), 
            'limit' => $this->getLimit()
        ];
    }
}
