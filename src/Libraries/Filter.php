<?php

namespace MusementSdk\Libraries;
/**
 * Filter class library
 */
class Filter {
    static $headerParams = ['Accept-Language'];
    
    private $params = [];
    
    /**
     * Setting params
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value){
        $this->params[$name] = $value;
    }
    
    /**
     * Get params only for url
     * @return array
     */
    public function getParams() {        
        return array_diff_key($this->params, self::$headerParams);
    }
    
    /**
     * Get params only for header
     * @return array
     */
    public function getHeaderParams(){
        return array_intersect_key($this->params, array_flip(self::$headerParams));
    }
}
